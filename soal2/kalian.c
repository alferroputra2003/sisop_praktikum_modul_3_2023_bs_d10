 #include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>   
#include <stdlib.h> 

int main()
{
    srand((time(NULL));

        //inisialisasi array
        int arrayA[4][2];
        int arrayB[2][5];
        int arrayResult[4][5];

        //matriks A 4 x 2
        for(int i=0; i<4; i++)
        {
            for(int j=0;j<2;j++)
            {
                arrayA[i][j] = rand() % 5 + 1;
            }
        }

        printf("\n Matriks A\n");
        
        for(int i=0; i<4; i++)
        {
            for(int j=0;j<2;j++)
            {
                printf("%d ", arrayA[i][j]);
            }
            printf("\n");
        }

        //Matriks B 2 x 5
        for(int i=0; i<2; i++)
        {
            for(int j=0;j<5;j++)
            {
                arrayB[i][j] = rand() % 4 + 1;

            }
        }

        printf("\n Matriks B\n");
        
        for(int i=0; i<2; i++)
        {
            for(int j=0;j<5;j++)
            {
                printf("%d ", arrayB[i][j]);
            }
            printf("\n");
        }


        printf("\nResult\n");
        //perkalian matriks
        for(int i=0; i<4; i++)
        {
            for(int j=0; j<5; j++)
            {
                arrayResult[i][j]=0;
                for(int k=0; k<2;k++)
                {
                    arrayResult[i][j]+= arrayA[i][k] * arrayB[k][j];
                }
            }
        }

        for(int i=0; i<4; i++)
        {
            for(int j=0;j<5;j++)
            {
                printf("%d ", arrayResult[i][j]);
            }
            printf("\n");
        }

        
        key_t key = 111094;
        int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
        int (*arrayShared)[5] = shmat(shmid, NULL, 0);

        for(int i=0; i<4;i++)
        {
            for(int j = 0; j<5; j++)
            {
               arrayShared[i][j] = arrayResult[i][j];
            }
        }

        printf("\n shared \n");

        for(int i=0; i<4;i++)
        {
            for(int j = 0; j<5; j++)
            {
               printf("%d ", arrayShared[i][j]);
            }
            printf("\n");
        }
        shmdt((void *) arrayShared);

}
