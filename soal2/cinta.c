#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>   
#include <stdlib.h> 
#include <pthread.h>

void *factorial(void*args)
{

    unsigned long long int result=1;
    int angka;
    angka = *((unsigned long long int *) args);

    for(long int i = 1; i <= angka; i++)
    {
        result = result*i;
    }
    printf("%llu ", result);
    return NULL;

}

int main()
{
    key_t key = 111094;

    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayShared)[5] = shmat(shmid, NULL, 0);

    printf("\n shared \n");

    for(int i=0; i<4;i++)
    {
        for(int j = 0; j<5; j++)
        {
            printf("%d ", arrayShared[i][j]);
        }
        printf("\n");
    }

    printf("\n");
    pthread_t t_id[4];

    for(int i=0;i < 4; i++)
    {
        for(int j=0; j < 5; j++)
        {
            pthread_create(&t_id[i], NULL, &factorial, &arrayShared[i][j]);
            pthread_join(t_id[i], NULL);
        }
        printf("\n");
    }

    for (int i=0 ; i<4; i++) 
    {
	    pthread_join(t_id[i], NULL);
	}

    shmdt((void *) arrayShared);
}