#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>   
#include <stdlib.h> 
#include <pthread.h>

void factorial(int a)
{
    unsigned long long int result=1;
    for(int i = 1; i <= a; i++)
    {
        result = result*i;
    }
    printf("%llu ", result);
}

int main()
{
    key_t key = 111094;

    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayShared)[5] = shmat(shmid, NULL, 0);

    printf("\n shared \n");

    for(int i=0; i<4;i++)
    {
        for(int j = 0; j<5; j++)
        {
            printf("%d ", arrayShared[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    for(int i=0;i < 4; i++)
    {
        for(int j=0; j < 5; j++)
        {
            factorial(arrayShared[i][j]);
        }
        printf("\n");
    }

    shmdt((void *) arrayShared);
    shmctl(shmid, IPC_RMID, NULL);
}