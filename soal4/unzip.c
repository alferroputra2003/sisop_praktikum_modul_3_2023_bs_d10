#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

pid_t pid;
int status;

void download() {
    while ((wait(&status)) > 0);
    pid = fork();
    if (pid == 0) {
        char link[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
        execl("/usr/bin/wget", "wget", link, "-O", "hehe.zip", NULL);
        exit(0);
    }

    while ((wait(&status)) > 0);
}

void unzip() {
    while ((wait(&status)) > 0);
    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/unzip", "unzip", "hehe.zip", NULL);
        exit(0);
    }

    while ((wait(&status)) > 0);
}

int main() {
    download();
    unzip();
    
    return 0;
}

