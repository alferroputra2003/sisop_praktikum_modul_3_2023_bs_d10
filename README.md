# sisop_praktikum_modul_3_2023_BS_D10



## Nomor 1
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <ctype.h>

#define MAX_CHAR 256
#define MAX_TREE_HT 50

char send[26];

struct MinHeapNode
{
   char data;             // Karakter pada file
   unsigned freq;         // Frekuensi karakter
   struct MinHeapNode* left;
   struct MinHeapNode* right;
};

struct MinHeap {
  unsigned size;
  unsigned capacity;
  struct MinHeapNode **array;
};

struct MinHeapNode *newNode(char data, unsigned freq) {
  struct MinHeapNode *temp = (struct MinHeapNode *)malloc(sizeof(struct MinHeapNode));

  temp->left = temp->right = NULL;
  temp->data = data;
  temp->freq = freq;

  return temp;
}

struct MinHeap *createMinH(unsigned capacity) {
  struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

  minHeap->size = 0;

  minHeap->capacity = capacity;

  minHeap->array = (struct MinHeapNode **)malloc(minHeap->capacity * sizeof(struct MinHeapNode *));
  return minHeap;
}

void swapMinHeapNode(struct MinHeapNode **a, struct MinHeapNode **b) {
  struct MinHeapNode *t = *a;
  *a = *b;
  *b = t;
}

void minHeapify(struct MinHeap *minHeap, int idx) {
  int smallest = idx;
  int left = 2 * idx + 1;
  int right = 2 * idx + 2;

  if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
    smallest = left;

  if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
    smallest = right;

  if (smallest != idx) {
    swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
    minHeapify(minHeap, smallest);
  }
}

int checkSizeOne(struct MinHeap *minHeap) {
  return (minHeap->size == 1);
}

struct MinHeapNode *extractMin(struct MinHeap *minHeap) {
  struct MinHeapNode *temp = minHeap->array[0];
  minHeap->array[0] = minHeap->array[minHeap->size - 1];

  --minHeap->size;
  minHeapify(minHeap, 0);

  return temp;
}

void insertMinHeap(struct MinHeap *minHeap, struct MinHeapNode *minHeapNode) {
  ++minHeap->size;
  int i = minHeap->size - 1;

  while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
    minHeap->array[i] = minHeap->array[(i - 1) / 2];
    i = (i - 1) / 2;
  }
  minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap) {
  int n = minHeap->size - 1;
  int i;

  for (i = (n - 1) / 2; i >= 0; --i)
    minHeapify(minHeap, i);
}

int isLeaf(struct MinHeapNode *root) {
  return !(root->left) && !(root->right);
}

struct MinHeap *createAndBuildMinHeap(char data[], int freq[], int size) {
  struct MinHeap *minHeap = createMinH(size);

  for (int i = 0; i < size; ++i)
    minHeap->array[i] = newNode(data[i], freq[i]);

  minHeap->size = size;
  buildMinHeap(minHeap);

  return minHeap;
}

struct MinHeapNode *buildHuffmanTree(char data[], int freq[], int size) {
  struct MinHeapNode *left, *right, *top;
  struct MinHeap *minHeap = createAndBuildMinHeap(data, freq, size);

  while (!checkSizeOne(minHeap)) {
    left = extractMin(minHeap);
    right = extractMin(minHeap);

    top = newNode('$', left->freq + right->freq);

    top->left = left;
    top->right = right;

    insertMinHeap(minHeap, top);
  }
  return extractMin(minHeap);
}

void printArray(int arr[], int n, char c) {
    FILE *fp = fopen("temp.txt", "a");

    if (fp == NULL) {
        printf("Error opening file\n");
        return;
    }

  int i;

  fprintf(fp, "%c", c);
  for (i = 0; i < n; ++i){
    fprintf(fp, "%d", arr[i]);
  }

  fprintf(fp, "%c", '\n');
  fclose(fp);
}

void printHCodes(struct MinHeapNode *root, int arr[], int top) {
  if (root->left) {
    arr[top] = 0;
    printHCodes(root->left, arr, top + 1);
  }
  if (root->right) {
    arr[top] = 1;
    printHCodes(root->right, arr, top + 1);
  }
  if (isLeaf(root) && strchr(send, root->data)!=NULL) {
    printArray(arr, top, root->data);
  }
}

void HuffmanCodes(char data[], int freq[], int size) {
  struct MinHeapNode *root = buildHuffmanTree(data, freq, size);

  int arr[MAX_TREE_HT], top = 0;

  printHCodes(root, arr, top);
}

int count_line(char filename[]){
    int count = 0;
    char line[512];
    FILE *fp = fopen(filename, "r");

    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    while (fgets(line, 512, fp) != NULL) {
        count++;
    }

    fclose(fp);

    return count;
}

int findLength(char c){
    char line[512];
    FILE *fp = fopen("temp.txt", "r");

    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    while (fgets(line, 512, fp) != NULL) {
    if (strchr(line, c) != NULL) {
        return strlen(line) - 1;
        }
    }

    fclose(fp);
    return -1;
}

int translate(char filename[]) {
    FILE *fp = fopen(filename, "a");
    FILE *fs = fopen("temp.txt", "r");
    FILE *fc = fopen("file.txt", "r");

    if (fp == NULL || fs == NULL || fc == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    char line[512];
    char c;
    int count = 0;
    while ((c = fgetc(fc)) != EOF) {
        fseek(fs, 0, SEEK_SET);
        c = toupper(c);
        while (fgets(line, 512, fs) != NULL) {
            if (line[0] == c) {
                char *sub = malloc(strlen(line));
                strcpy(sub, line + 1);
                count += strlen(sub)-1;
                sub[strlen(sub) - 1] = '\0';
                fwrite(sub, strlen(sub), 1, fp);
                free(sub);
                break;
            }
        }
    }


    fclose(fp);
    fclose(fs);
    fclose(fc);
    return count;
}

int main() {
    int fd1[2], fd2[2];
    pid_t pid;

    if (pipe(fd1) == -1 || pipe(fd2) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) { 
        char *arr = malloc(sizeof(char) * 26);
        int *arrint = malloc(sizeof(int) * 26);
        char *sent = malloc(sizeof(char)*26);
        close(fd1[1]);
        read(fd1[0], arr, sizeof(char) * 26);
        read(fd1[0], arrint, sizeof(int) * 26);
        read(fd1[0], sent, 26);
        close(fd1[0]);

        strcpy(send, sent);

        close(fd1[0]);
        HuffmanCodes(arr, arrint, sizeof(char)*26);

        close(fd2[0]);
        int countLine = count_line("temp.txt");
        write(fd2[1], &countLine, sizeof(countLine));
        FILE *fp = fopen("temp.txt", "r");
            if(fp==NULL){
                perror("fail to open");
                exit(1);
            }

            char line[512];
            while(fgets(line, 512, fp)!= NULL){
                write(fd2[1], &line, sizeof(line));
            }
        close(fd2[1]);
    } else { 
        FILE *fp;
        char filename[100];
        char c;

        fp = fopen("file.txt", "r");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        int freq[26] = {0}; 
        char alpha[26] = {'\0'}; 
        int idx = 0; 
        while ((c = fgetc(fp)) != EOF) {
            if (isalpha(c)) { 
                c = toupper(c); 
                int pos = c - 'A'; 
                freq[pos]++; 
                if (freq[pos] == 1) { 
                    alpha[idx] = c; 
                    idx++; 
                }
            }
        }

        char *arr = malloc(sizeof(char) * 26);
        int *arrfr = malloc(sizeof(int) * 26);

        int j = 0;
        for (int i = 0; i < 26; i++) {
            alpha[i] = 'A' + i;
            arrfr[i] = freq[i];

            if(freq[i]!=0){
                send[j] = alpha[i];
                j++;
            }
        }

        for (int i = 0; i < 26; i++) {
            arr[i] = alpha[i];
        }

        fclose(fp);

        close(fd1[0]);
        write(fd1[1], arr, sizeof(char) * 26);
        write(fd1[1], arrfr, sizeof(int) * 26);
        write(fd1[1], send, sizeof(send));
        close(fd1[1]);

        close(fd2[1]);
        int n;
        read(fd2[0], &n, sizeof(int));

        char coded[n][512];
        for (int i = 0; i < n; i++) {
            read(fd2[0], coded[i], sizeof(char) * 512);
        }

        close(fd2[0]);
        int number = 0;
        for(int i = 0; i<26; i++){
            int calc;
            if(freq[i]!=0){
                calc = freq[i]*8;
                number += calc;
            }
        }

        printf("Jumlah bit SEBELUM dilakukan kompresi adalah %d\n", number);

        number = translate("translated.txt");;
        printf("Jumlah bit SETELAH dilakukan kompresi adalah %d\n", number);

        system("rm temp.txt");
    }

    return 0;
}
```
Untuk nomor 1, langkah pertama adalah membuat strung untuk node dari tree tersebut. setelah itu kita membuat struct untuk minHeap dan juga Minheap Node. Setelah itu kita membuat beberapa fungsi berupa create minh, swapminhheapnode untuk menukar, minheapify, checksizeOne, extract, insert, isleaf untuk mengecek leaf dari node, kemudian ada createand build minheap untuk membuat minheap, kemudian ada buildhuffmantree untuk membangun huffmann tree itu sendiri. Kita juga memiliki fungsi dengan tujuan untuk mengeprint array. Selain itu juga terdapat fungsi berguna huntuk mengeprint huffmancode nya itu sendiri. Pada fungsi main, pertama kita nyatakan 2 pipe dan juga kita buat pernyataan untuk menjelaskan apabila terdapat failure dalam pembuatan pipe. kemudian pada child proses, kita akan menggunakan pipe yang kita nyatakan tadi untuk membaca arr dan arrint yang kemudian kita close. setelah itu kita menggunkaan strcpy untuk menngopu send ke sent. setelah itu kita panggil fungsi huffman code. langkah selanjutnya adalah menyatakan line yang ada menggunakan count_line pada file.txt yang mana hasilnya akan kita masukkan kedalam pipe fd2. Setelah itu kita akan menyatakan *fp sebagai file temp.txt yang mana akan kita buka. Pada Parent prosess, kita nyatakan file fp, filename, dan juga c. kemudian kita buka file fp nya. setelah itu kita nyatakan freq sebagai 0 dan char alpha sebagai \0. Setelah itu, selama nilai dgetc dari file bukanlah akhir dari file, maka kita akan melakukan pengecekan isalpha terhadap karakter c. Kemudian kira lakukan looping for i sampai i< 26 atau jumlah alphabet yang mana nilai dari alpha i adalah A + i itu sendiri. kemudian di akhir kita sisa melakukan close dan melakukan write kepada pipe fd1 mengenai nilai dari arr, arrfr dan send. kemudian kita close. Setelah itu, kita akan melakukan printing sesuai dengan hasil bit sebelum dan sesudah dan diakhiri dengan system rm temp.txt


## Nomor 2
### Kalian.c

```
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>   
#include <stdlib.h> 


int main()
{
    srand((time(NULL));

        //inisialisasi array
        int arrayA[4][2];
        int arrayB[2][5];
        int arrayResult[4][5];

        //matriks A 4 x 2
        for(int i=0; i<4; i++)
        {
            for(int j=0;j<2;j++)
            {
                arrayA[i][j] = rand() % 5 + 1;
            }
        }

        printf("\n Matriks A\n");
        
        for(int i=0; i<4; i++)
        {
            for(int j=0;j<2;j++)
            {
                printf("%d ", arrayA[i][j]);
            }
            printf("\n");
        }

        //Matriks B 2 x 5
        for(int i=0; i<2; i++)
        {
            for(int j=0;j<5;j++)
            {
                arrayB[i][j] = rand() % 4 + 1;

            }
        }

        printf("\n Matriks B\n");
        
        for(int i=0; i<2; i++)
        {
            for(int j=0;j<5;j++)
            {
                printf("%d ", arrayB[i][j]);
            }
            printf("\n");
        }


        printf("\nResult\n");
        //perkalian matriks
        for(int i=0; i<4; i++)
        {
            for(int j=0; j<5; j++)
            {
                arrayResult[i][j]=0;
                for(int k=0; k<2;k++)
                {
                    arrayResult[i][j]+= arrayA[i][k] * arrayB[k][j];
                }
            }
        }

        for(int i=0; i<4; i++)
        {
            for(int j=0;j<5;j++)
            {
                printf("%d ", arrayResult[i][j]);
            }
            printf("\n");
        }

        
        key_t key = 111094;
        int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
        int (*arrayShared)[5] = shmat(shmid, NULL, 0);

        for(int i=0; i<4;i++)
        {
            for(int j = 0; j<5; j++)
            {
               arrayShared[i][j] = arrayResult[i][j];
            }
        }

        printf("\n shared \n");

        for(int i=0; i<4;i++)
        {
            for(int j = 0; j<5; j++)
            {
               printf("%d ", arrayShared[i][j]);
            }
            printf("\n");
        }
        shmdt((void *) arrayShared);

}
```
Di awal code, perlu dinyatakan matriksnya terlebih dahulu, berdasarkan soal terdapat dua jenis matriks yaitu 4x2 dan 2x5. Kemudian karena input dari code sendiri merupakan nilai yang acak, digunakan fungsi rand() dengan tujuan mengacak nilai sesuai keinginan kita yaitu 1-5 dan 1-4. Setelah itu, kita lakukan perkalian pada matriks dan kita masukkan nilai hasil perkalian ke matriks baru. Setelah itu karena diperlukannya hasil untuk shared memory, maka kita nyatakan terlebih dahulu key nya. kemudian kita dapatkan shared memorynya menggunakan shmid = shmget. Setelah itu, kita pindahkan nilai array perkalian kedalam array shared nya menggunakan forloop. setelah itu code ditutup dengan shmdt.

### cinta.c
```
void *factorial(void*args)
{

    unsigned long long int result=1;
    int angka;
    angka = *((unsigned long long int *) args);

    for(long int i = 1; i <= angka; i++)
    {
        result = result*i;
    }
    printf("%llu ", result);
    return NULL;

}

int main()
{
    key_t key = 111094;

    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayShared)[5] = shmat(shmid, NULL, 0);

    printf("\n shared \n");

    for(int i=0; i<4;i++)
    {
        for(int j = 0; j<5; j++)
        {
            printf("%d ", arrayShared[i][j]);
        }
        printf("\n");
    }

    printf("\n");
    pthread_t t_id[4];

    for(int i=0;i < 4; i++)
    {
        for(int j=0; j < 5; j++)
        {
            pthread_create(&t_id[i], NULL, &factorial, &arrayShared[i][j]);
            pthread_join(t_id[i], NULL);
        }
        printf("\n");
    }

    for (int i=0 ; i<4; i++) 
    {
	    pthread_join(t_id[i], NULL);
	}

    shmdt((void *) arrayShared);
}
```
pada cinta.c dilakukannya faktorial menggunakan thread, langkah pertama adalah melakukan pembuatan fungsi faktorial. pastikan bahwa fungsi merupakan pointer. Kemudian kita nyatakan key yang sama dengan yang berada di kalian.c dan kita ambil shared memory menggunakan shmid = shmget. setelah itu kita nyatakan pthread[4] karena akan terdapat 4 thread. kemudian menggunakan looping, kita jalankan thread tersebut terhadap nilai array nya.

### sisop.c
```
void factorial(int a)
{
    unsigned long long int result=1;
    for(int i = 1; i <= a; i++)
    {
        result = result*i;
    }
    printf("%llu ", result);
}

int main()
{
    key_t key = 111094;

    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayShared)[5] = shmat(shmid, NULL, 0);

    printf("\n shared \n");

    for(int i=0; i<4;i++)
    {
        for(int j = 0; j<5; j++)
        {
            printf("%d ", arrayShared[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    for(int i=0;i < 4; i++)
    {
        for(int j=0; j < 5; j++)
        {
            factorial(arrayShared[i][j]);
        }
        printf("\n");
    }

    shmdt((void *) arrayShared);
    shmctl(shmid, IPC_RMID, NULL);
}
```
Pada bagian ini, dilakukan faktorial tanpa thread sehingga yang perlu dilakukan hanyalah pembuatan fungsi biasa. Nilai dari array yang berada di shared memory sisa di loop kedalam fungsi tersebut. setelah itu untuk menutup shared memory kita nyatakan shmctl. 

Perbedaan antara penggunaan Thread dan tidak adalah Penggunaan thread akan jalan lebih cepat dikarenakan proses berjalan bersamaan, namun ini akan berdampak pada hasil yang keluar. maka dari itu perlu dipastikan penambahan thread join agar seluruh thread dipastikan selesai. 


## Nomor 3
## Nomor 4
### unzip.c

```
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

pid_t pid;
int status;

void download() {
    while ((wait(&status)) > 0);
    pid = fork();
    if (pid == 0) {
        char link[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
        execl("/usr/bin/wget", "wget", link, "-O", "hehe.zip", NULL);
        exit(0);
    }

    while ((wait(&status)) > 0);
}

void unzip() {
    while ((wait(&status)) > 0);
    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/unzip", "unzip", "hehe.zip", NULL);
        exit(0);
    }

    while ((wait(&status)) > 0);
}

int main() {
    download();
    unzip();
    
    return 0;
}
```
Pada bagian ini pertama kita akan menyatakan pid dan status, kemudian kita membuat fungsi untuk melakukan download dimana kita akan melakukan forking, dan menggunakan execl untuk mendownload file. pada file void unzip kita akan melakukan unzip dimana kkita akan menggunakan execl kembali. kemudian pada main kita sisa memanggil download dan unzip itu.

### categorize.c
```
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>
#include <math.h> 

#define MAX_LEN 256
#define MAX_PATH 256
#define MAX_EXT 5
#define MAX_DEST_SIZE 475 // maximum size of destination buffer

pthread_mutex_t lock;
FILE* log_file;

struct ThreadArgs {
    char dirname[MAX_LEN];
    int maxFile;
};

void writeLogMade(char createdDir[]){
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logMADE[256] = "MADE ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMADE, createdDir);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logMADE);
    pthread_mutex_unlock(&lock);
}

void writeLogAccess(char folderPath[]){
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logAccess[256] = "ACCESSED ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logAccess, folderPath);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logAccess);
    pthread_mutex_unlock(&lock);
}


void* create_directory(void* arg) {
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg;
    char* dirname = thread_args->dirname;
    int maxFile = thread_args->maxFile;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    // membuat nama direktori baru dengan menambahkan prefix "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);

    //hitung banyak file
    char countFile[256];
    char hehe_dir[20] ="files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE* fp;
    fp = popen(cmd, "r");
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
    
    //menghitung total direktori yang dibuat per ext [@todo, check if maxFile=0]
    int numDir = numFiles / maxFile ;

    if (numFiles % maxFile > 0)
        numDir = numDir + 1;

    // Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);

    //create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
    // directory creation was successful, so do something
        writeLogMade(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
         // mendefinisikan variable nama directory dengan suffix sesuai dengan index
         char _dirName[30];
         strcpy(_dirName, new_dirname);
         if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       

        // memeriksa apakah direktori sudah ada
        if (stat(_dirName, &st) == 0) {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        } else {
                // membuat direktori jika belum ada
                if (mkdir(_dirName, 0777) == 0) {
                    writeLogMade(_dirName);
                    //move files according to their extensions
                    char command1[600];
                    char command2[500];
                    char command3[100];
                    char logAccessSource[200];
                    char logAccessTarget[100];

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);
                    sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

                    system(command1);


                } else {
                    perror("Gagal membuat direktori");
                    
                }
            }
    }
    return NULL;
}


int main() {
    // membuka file log
    log_file = fopen("log.txt", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        exit(1);
    }

    // inisialisasi mutex lock
    if (pthread_mutex_init(&lock, NULL) != 0) {
        perror("Failed to initialize mutex lock");
        exit(1);
    }

//CREATE CATEGORIZED DIRECTORY
    char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    char other_dirname[MAX_LEN] = "other";
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;

    // membuka file untuk dibaca
    fp = fopen(filename, "r");

    // memeriksa apakah file berhasil dibuka
    if (fp == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    // membuat direktori "categorized" jika belum ada
    if (stat("categorized", &st) != 0) {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }

    // membuat direktori "other" di dalam "categorized" jika belum ada
    if (stat("categorized/other", &st) != 0) {
        writeLogMade("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }

    char maxfilename[50] = "max.txt";
    //open max.txt
    FILE *max;
    // membuka file untuk dibaca
    max = fopen(maxfilename, "r");

    // memeriksa apakah file berhasil dibuka
    if (max == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

     // menutup file
    fclose(max);

    // membaca nama direktori dari file, satu per satu
    while (fgets(dirname, MAX_LEN, fp)) {
        // menghilangkan karakter newline di akhir string
        dirname[strcspn(dirname, "\n")] = 0;

        // menghapus spasi di akhir string
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1])) {
            len--;
        }
        dirname[len] = '\0';

        // membuat direktori lain secara asynchronous dengan menggunakan thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFile = maxFile;
        pthread_create(&tid[i], NULL, create_directory, &thread_args[i]);
        i++;
    }

    // join thread yang masih berjalan
    for (int j = 0; j < i; j++) {
        pthread_join(tid[j], NULL);
    }

    // move other files to categorized/other
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);
    
    system(logAccessSource);
    system(logAccessTarget);

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);

    //output sort buffer.txt
    system("sort buffer.txt");
    system("rm -rf buffer.txt");

    // count how many files in dir categorized/other
    DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir (otherPath)) != NULL) {
        writeLogAccess(otherPath);
        while ((ent = readdir (dir)) != NULL) {
            if(ent->d_type == DT_REG) {
                count++;
            }
        }
        closedir (dir);
    } else {
        perror ("Tidak bisa membuka direktori");
        return 1;
    }
    printf("extension_other: %d\n", count);

    // menutup file
    fclose(fp);
    
   // menghancurkan mutex lock
    pthread_mutex_destroy(&lock);

    // menutup file log
    fclose(log_file);

    return 0;
}
```
Pada bagian ini terdapat beberapa fungsi yang kita buat, pertama da write logmade untuk menulis log pesan ketika dibuatnya sesuatu. writelogAccess untuk menulis log pesan ketika ada yang melakukan access, create directory berguna untuk membuat directory.  Pada fungsi pembuatan direktori akan dilakukan banyak pengerjaan seperti pengecekan per direktori, membuat direktori, dan memindahkan file sesuai dengan extensionsnya. Pada main, kita pertama akan membuka file log txt terlebih dahulu dan melakukan inisialisasi mutex lock. setelah itu akan dilakukan nya pembuatan categorized directory yang mana pertama akan kita nyatakan file name, dirname, dan other, file, strict, pthread, struct threadargs dan int i = 0. kemudian file kita buka, setelah itu kita akan lakukan pembuatan categorized direktori, dan membuat direktory other jika belom ada, setleah itu kita akan membuka file max.txt. langkah selanjutka kita akan membuat direktori lain secara asinkron menggunakan thread, kita akan lakukan join. langkah selanjutnya adalah memindah files keadalam direktori masing masing, kemudian output akan berupa sort buffer dan kita juga akan menghitung jumlah file yang ada di masing masing kategori.

### logchecker.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

```
int main() {
    // Accessed count
    printf ("\e[33mTotal banyak akses:\e[0m\n");
    system("grep ACCESSED log.txt | wc -l");

    // List folders & total file
    printf ("\e[33m\nList folder & total file tiap folder:\e[0m\n");
    system("grep 'MOVED\\|MADE' log.txt \
    | awk -F 'categorized' '{print \"categorized\"$NF}' \
    | sed '/^x$/d' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");


    // total file tiap extension terurut secara ascending
    printf ("\n\e[33mTotal file tiap extension\e[0m\n");
    system("grep 'MADE\\|MOVED' log.txt \
    | grep -o 'categorized/[^o]*' \
    | sed 's/categorized\\///' \
    | sed '/^$/d' \
    | sed 's/ \\([^o]*\\)//' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \" \"=\" \" \" $1-1}'");

    return 0;
}
```

pada bagian ini kita akan lakukan print terhadap accessed count listfolder dan total file, hal ini kita lakukan menggunakan command line system sehingga kita bisa menggunakan command command terminal.






